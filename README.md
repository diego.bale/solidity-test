**Solidity Test**  
(https://hackmd.io/sE46RpdHR7i3fRi-xH9kQg)


**G$ solidity test**  

**Q1**  

Write two methods that calculate the exchange of two tokens.
one that accepts value of X and returns how many Y you would get 
for it and vise versa

    Token X is a two decimals token, while token Y is 18 decimals token.
    Their exchange rate is 0.35Y=1X.  

**ANSWER**: src/ExchangeContract.sol 
<br><br><br> 
  
  
  
  

**Q2**

Write a method that if user proves he owns the private key for public address X an event is emitted.

    event: Owner(address owner, address owned) where owner is the msg.sender and owned=X (msg.sender should be different than X)
    make sure the proof can not be replayed or front run. ie if a attacker watches TXs submitted to the blockchain he will not be able to use the proof himself by submitting it faster (ie with higher gas fee)


**ANSWER**:   
To generate signature and identity (Javascript): *src/signSomething.js*  
To verify signature (Solidity): *src/VerifySignature.sol*    