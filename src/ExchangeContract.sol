// SPDX-License-Identifier: MIT

pragma solidity >=0.4.22 <0.7.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/math/SafeMath.sol";

contract ExchangeContract {
    
    using SafeMath for uint256;
    
    uint public constant equivTo1tknX = 10**2;
    uint public constant equivTo1tknY = 10**18;
    uint public constant mulpXdenomY = 35;
    uint public constant mulpYdenomX = 100;
    
    // Relation = 100 XTK ==> 35 YTK
    
    // FUNCTION Receives values in weiX where 100 weiX is equal to 1 X token (XTK)
    // Returns weiY value
    function exchangeXforY(uint numXtokens) public pure returns (uint) {
        uint toWeiY = numXtokens.mul(mulpXdenomY).mul(equivTo1tknY);
        return toWeiY.div(mulpYdenomX).div(equivTo1tknX);
    }
    
    // FUNCTION Receives values in weiY where 1000000000000000000 weiY is equal to 1 Y token (YTK)
    // Returns weiX value
    function exchangeYforX(uint numYtokens) public pure returns (uint) {
        uint toWeiX = numYtokens.mul(mulpYdenomX).mul(equivTo1tknX);
        return toWeiX.div(mulpXdenomY).div(equivTo1tknY);
    }
    
    // NOTE: Doing all the multiplications FIRST can cause an overflow if the numbers are way too big
    // but it's not quite usual to have that kind of numbers and is covered by SafeMath. 
    // I did this way because those chances are very low and, more important, you can LOSE precision
    // if the division come first
    
}

\\ TRANSACTION
\\ https://rinkeby.etherscan.io/tx/0x31c7d5b134972be4f71cf020da47290ebda80f26b892748cfcfd63075ceb7de6
\\ DEPLOYED CONTRACT ADDRESS
\\ 0xcadb9dbec598e82adb6f3f047ce9e6e0de746811





