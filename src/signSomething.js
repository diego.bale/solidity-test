const EthCrypto = require("eth-crypto");
const Web3 = require("web3");

// using Rinekby
const provider = new Web3.providers.HttpProvider("https://rinkeby.infura.io/v3/5dcd1d1dbdef433f88f8a23dc862f656");
const web3 = new Web3(provider);

const signerIdentity = EthCrypto.createIdentity();

const hashedMessage = web3.utils.soliditySha3("0xab5013298c85d0E024C566B3ae46033D1f447D4e"); 
// This is an address I own. I will use it to execute the function in the contract to return true

const signedMessage = EthCrypto.sign(signerIdentity.privateKey, hashedMessage);

console.log(`\nHashedMessage : ${hashedMessage}`);
console.log(`\nSignature msg : ${signedMessage}`);
console.log(`Expected Address: ${signerIdentity.address}\n`);


// This JS code will create an identity with its private key and address
// It will also hash a message containing another address
// This identity will sign the message with the private key
// The signature of the message is generated 
// It will print the hashed message
// It will print the signature of the message and the address corresponding to the private key used to sign
// This last two parameters should be the input of the contract function
