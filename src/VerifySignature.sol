// SPDX-License-Identifier: MIT

pragma solidity >=0.4.22 <0.7.0;

import "https://github.com/OpenZeppelin/openzeppelin-contracts/blob/master/contracts/cryptography/ECDSA.sol";

contract VerifySignature {
    using ECDSA for bytes32;

    event Owner(address owner, address owned);
    
    function verifyMsg(bytes memory signature, address expectedSignerAddress) public {
        
        bytes32 hashedMessage = keccak256(abi.encodePacked(msg.sender));
        
        address signerAddress = hashedMessage.recover(signature);
        
        require (signerAddress == expectedSignerAddress, "Not match");
        
        emit Owner(msg.sender, signerAddress);
    }
}

// This contract will receive the expected address and the signature of the msg that address signed with its private key
// The signed message IS the address of the msg sender (it should be, if not the verify will fail)
// It will hash the address of the message sender and then used it to recover the signature received
// If it's a match it means the message sender owns also the private key of the signed message (its current address) and the contract will emit an event
// If it's not a match the transaction will fail because of the require

// NOTE 1: To prevent the same msg sender to emit unlimited events by calling the Verify. A mapping could be added to store each address already verified associated to each msg.sender

// NOTE 2: It also can be added the "\x19Ethereum Signed Message:\n32" prefix to the message to specify a unique service
